import { history } from 'basic'
import { getSuggestionApi, option } from '../apis'

@Component
export struct SearchHistory {
  @State isDeleting: boolean = false
  @State q: string = ''
  @State qs: string[] = []
  onSearch: (q: string) => void = () => {
  }
  @State suggestionsList: string[] =
    ['JavaScript 基础', 'os模块的简单使用', 'Vue.js简介', 'css线性渐变', 'Vue.js简介', 'css线性渐变']

  aboutToAppear(): void {
    this.qs = history.getAll()
  }

  build() {
    // 搜索历史
    Flex({ direction: FlexDirection.Row, wrap: FlexWrap.Wrap }) {
      Row() {
        Text('搜索历史')
          .fontSize(15)
          .fontColor('#a3a3a3')
        Blank()
        if (this.isDeleting) {
          Text() {
            Span('清除全部')
              .onClick(async () => {
                await history.clear()
                this.qs = []
              })
            Span(' | ')
            Span('完成')
              .onClick(() => {
                this.isDeleting = false
              })
          }
          .fontSize(14)
          .fontColor('#a3a3a3')
        } else {
          Image($r('app.media.garbage_can'))
            .width(16)
            .aspectRatio(1)
            .fillColor('#a3a3a3')
            .onClick(() => {
              this.isDeleting = true
            })
        }
      }
      .width('100%')

      ForEach(this.qs, (q: string) => {
        Row({ space: 8 }) {
          Text(q)
            .fontSize(14)
            .fontColor('#6F6F6F')
            .textAlign(TextAlign.Center)
            .padding({
              left: 5,
              right: 5,
              top: 3,
              bottom: 3
            })
          if (this.isDeleting) {
            Image($r('app.media.close'))
              .width(12)
              .aspectRatio(1)
              .fillColor('#878787')
              .onClick(async () => {
                await history.delItem(q)
                this.qs = history.getAll()
              })
          }
        }
        .padding({ left: 12, right: 12 })
        .backgroundColor('#f3f4f5')
        .borderRadius(16)
        .margin({ right: 16, top: 16 })
        .onClick(() => {
          if (!this.isDeleting) {
            this.onSearch(q)
          }
        })
      })
      Divider()
        .strokeWidth(2)
        .width('100%')
        .color('#edeef0')
        .padding({ top: 30 })
      // 猜你想搜
      Column({ space: 8 }) {
        Text('猜你想搜')
          .fontSize(14)
          .fontWeight(FontWeight.Bold)
        Text()
          .width(45)
          .height(2)
          .backgroundColor('#fe4066')
      }
      .padding({ top: 30 })

      // 内容推荐
      Flex({ wrap: FlexWrap.Wrap }) {
        ForEach(this.suggestionsList, (item: string, index: number) => {
          Row() {
            Text(item)
              .margin({ left: this.suggestionsList.length % 2 === 0 ? 30 : 0 })
            if (index % 2 === 0) {
              Text('|')
                .fontColor('#edeef0')
            }
          }
          .width('50%')
          .padding({ bottom: 15 })
          .justifyContent(FlexAlign.SpaceBetween)
        })
      }
      .width('100%')
      .padding({ top: 30 })
    }
    .padding(16)
  }
}