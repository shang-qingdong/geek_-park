import { promptAction, router } from '@kit.ArkUI'
import { history, JkHomeListItem, RequestAxios } from 'basic'
import { HomeView } from '../../../../Index'
import { getSearchApi, getSuggestionApi, listData, option, result } from '../apis'
import { SearchHistory } from '../commponents/SearchHistory'

@Preview
@Component
export struct SearchChannelView {
  @StorageProp("safeTop") topHeight: number = 0
  @State q: string = ''
  @State isSearch: boolean = false
  @State SearchList: result[] = []
  @State suggestionsList: option[] = []
  @State page: number = 1
  @State refreshIng: boolean = false

  async getSearchData() {
    const res = await getSearchApi(this.q)
    this.SearchList = res.data.data.results
  }

  async getSuggestionsData() {
    const res = await getSuggestionApi(this.q)
    this.suggestionsList = res.data.data.options
    // AlertDialog.show({ message: JSON.stringify(res.data.data.options) })
  }

  build() {
    Column({ space: 20 }) {
      // 搜索框
      Column() {
        Row() {
          Image($r('app.media.ic_public_left'))
            .width(30)
            .aspectRatio(1)
            .onClick(() => {
              router.back()
            })
          Search({ placeholder: '请输入关键字搜索', value: this.q })
            .placeholderFont({ size: 14 })
            .fontColor(Color.Red)
            .height(32)
            .width(260)
            .onSubmit(value => {
              this.q = value
              this.isSearch = true
              if (!this.q) {
                return promptAction.showToast({ message: '请输入搜索关键字' })
              }
              // 添加历史记录
              history.setItem(this.q)
              this.getSearchData()
            })
            .onChange(value => {
              this.q = value
              if (!value) {
                this.isSearch = false
              }
              this.getSuggestionsData()
            })
          Text('取消')
            .onClick(() => {
              this.q = ''
            })
        }
        .width('100%')
        .justifyContent(FlexAlign.SpaceBetween)
      }
      .width('100%')

      Column() {
        if (this.isSearch) {
          // 文章列表
          Refresh({ refreshing: $$this.refreshIng }) {
            List() {
              ForEach(this.SearchList, (item: result) => {
                JkHomeListItem({ item: item })
                  .onClick(() => {
                    router.pushUrl({
                      url: "pages/DetailPage",
                      params: {
                        id: item.art_id,
                        comm_count: item.comm_count
                      }
                    })
                  })
              })
            }
            .width('100%')
            .layoutWeight(1)
            .onReachEnd(async () => {
              const res = await getSearchApi(this.q)
              this.SearchList.push(...res.data.data.results)
              this.page++
            })
          }
          .onRefreshing(async () => {
            const res = await getSearchApi(this.q)
            this.SearchList = res.data.data.results
            this.page = 1
            this.refreshIng = false
          })

        } else {
          if (this.q === '') {
            //搜索历史
            SearchHistory({
              onSearch: (q: string) => {
                this.q = q
                this.isSearch = true
                this.getSearchData()
              }
            })
          } else {
            // 联想建议
            Column() {
              List() {
                ForEach(this.suggestionsList, (item: string, index: number) => {
                  ListItem() {
                    Row({ space: 10 }) {
                      Image($r('app.media.search_icon'))
                        .width(20)
                        .aspectRatio(1)
                        .fillColor(Color.Gray)
                      Text(item)
                    }
                    .width('100%')
                    .padding({ top: 5 })
                    .border({ width: { bottom: 2 }, color: '#d0d1d3' })
                  }
                  .onClick(() => {
                    this.q = item
                    history.setItem(this.q)
                    this.isSearch = true
                    this.getSearchData()
                  })
                })
              }
            }
          }
        }
      }
      .layoutWeight(1)
    }
    .width('100%')
    .height('100%')
    .alignItems(HorizontalAlign.Start)
    .padding({ right: 10, left: 10, top: this.topHeight + 10 })
  }
}