import { JkHomeListItem } from 'basic'
// import { channels, getListDataApi, getTabBarListApi, result } from '../apis'
// import { router } from '@kit.ArkUI'

import { channels, getAillChannel, getListDataApi, getMyChannelApi, result } from '../apis'
import { router } from '@kit.ArkUI'
import { SearchChannelView } from './SearchChannelView'

@Component
export struct HomeView {
  @StorageProp("safeTop") topheight: number = 0
  @State myChannel: channels[] = []
  @State optionalChannel: channels[] = []
  @State list: result[] = []
  @State
  @Watch("getListData")
  activeIndex: number = 0
  //半模态
  @State isShow: boolean = false
  //编辑
  @State isEditShow: boolean = true

  async getChannel() {
    const res = await getMyChannelApi()
    this.myChannel = res.data.data.channels

    const res1 = await getAillChannel()
    this.optionalChannel = res1.data.data.channels


    this.myChannel.forEach((My: channels) => {
      this.optionalChannel.forEach((Op: channels, index) => {
        if (Op.name === My.name) {
          this.optionalChannel.splice(index, 1)
          return
        }
      })

    })
  }

  async getListData() {
    const res = await getListDataApi(Number(this.myChannel[this.activeIndex].id))
    this.list = res.data.data.results
  }

  async aboutToAppear() {
    this.getChannel()
    this.getListData()


  }

  @Builder
  TabBarBuilder(item: string = "", index: number) {

    Stack({ alignContent: Alignment.Bottom }) {
      Text(item)
        .fontWeight(this.activeIndex === index ? 800 : 0)
        .fontSize(this.activeIndex === index ? 20 : 16)
        .fontColor(this.activeIndex === index ? $r("[basic].color.N8") : $r("[basic].color.N5"))
        .padding({ bottom: 19 })
      Text()
        .width(this.activeIndex === index ? 25 : 0)
        .height(4)
        .borderRadius(2)
        .backgroundColor($r("[basic].color.MR"))
        .animation({ duration: 100 })
    }
    .padding({ left: 32, right: this.myChannel.length - 1 === index ? 32 : 0 })
    .height("100%")

  }

  @Builder
  MyChannelBuilder() {
    Column() {
      Column() {
        //close关闭图标
        Row() {
          Image($r("app.media.close"))
            .width(16)
            .aspectRatio(1)
            .fillColor('#a5a6ab')
            .onClick(() => {
              this.isShow = false
            })
        }
        .width('100%')
        .justifyContent(FlexAlign.End)

        // 我的频道
        Row() {
          Row({ space: 8 }) {
            Text('我的频道:')
              .fontSize(18)
              .fontWeight(FontWeight.Bold)
            Text(this.isShow ? '点击进入频道' : '拖动排序')
              .fontSize(12)
              .fontColor('#a5a6ab')
          }
          .alignItems(VerticalAlign.Bottom)

          Text(this.isEditShow ? '编辑' : '保存')
            .fontSize(13)
            .fontColor(this.isEditShow ? '#fe4066' : '#ffffff')
            .padding({
              top: 4,
              bottom: 4,
              right: 15,
              left: 15
            })
            .backgroundColor(this.isEditShow ? '#ffffff' : '#fc6627')
            .border({ width: 1, color: '#fe4066', radius: 15 })
            .onClick(() => {

              this.isEditShow = !this.isEditShow
            })
        }
        .width('100%')
        .padding({ top: 16 })
        .justifyContent(FlexAlign.SpaceBetween)
      }
      .padding({ bottom: 20 })


      // 我的频道下面的数组
      Flex({ wrap: FlexWrap.Wrap }) {
        ForEach(this.myChannel, (item: channels, index: number) => {
          Stack({ alignContent: Alignment.TopEnd }) {
            Text(item.name)
              .padding(8)
              .textAlign(TextAlign.Center)
              .fontColor(this.activeIndex === index ? '#fc6627' : '#3a3948')
              .backgroundColor($r("[basic].color.N2"))
              .borderRadius(15)
              .margin({ bottom: 15, right: 10 })
              .onClick(() => {
                if (this.isEditShow === true) {
                  this.activeIndex = index
                  this.isShow = false
                }

              })

            if (this.isEditShow === false) {
              if (this.activeIndex !== index) {
                Image($r('app.media.fillClose'))
                  .width(18)
                  .aspectRatio(1)
                  .fillColor('#a5a6ab')
                  .onClick(async () => {
                    this.myChannel.splice(index, 1)
                    if (this.activeIndex > index) {
                      this.activeIndex--
                    }
                    this.optionalChannel.push(item)
                  })
              }
            }
          }

        })
      }

      // 可选频道
      Row() {
        Row({ space: 8 }) {
          Text('可选频道：')
            .fontSize(18)
            .fontWeight(FontWeight.Bold)
        }
        .alignItems(VerticalAlign.Bottom)
      }
      .width('100%')
      .padding({ top: 16, bottom: 15 })
      .justifyContent(FlexAlign.SpaceBetween)

      // 可选频道下面的数组
      Flex({ wrap: FlexWrap.Wrap }) {
        ForEach(this.optionalChannel, (item: channels, index) => {
          Text("+  " + item.name)
            .textAlign(TextAlign.Center)
            .fontColor('#3a3948')
            .backgroundColor($r("[basic].color.N2"))
            .borderRadius(15)
            .padding(8)
            .margin({ bottom: 15, right: 10 })
            .onClick(() => {
              this.optionalChannel.splice(index, 1)
              this.myChannel.push(item)
            })
        })
      }

    }
    .padding({ top: this.topheight, left: 15, right: 15 })
    .width("100%")
    .height("100%")
    .backgroundColor($r("[basic].color.N1"))

  }

  build() {

    Column() {
      Stack({ alignContent: Alignment.TopEnd }) {
        Tabs({ index: $$this.activeIndex }) {
          ForEach(this.myChannel, (item: channels, index) => {
            TabContent() {
              List() {
                ForEach(this.list, (item: result) => {
                  JkHomeListItem({ item: item })
                    .onClick(() => {
                      router.pushUrl({
                        url: "pages/DetailPage",
                        params: {
                          id: item.art_id,
                          comm_count: item.comm_count
                        }
                      })
                    })
                })
              }
              .divider({ strokeWidth: 1, color: $r("[basic].color.N4") })
              .edgeEffect(EdgeEffect.None)
              .height("100%")
              .onReachEnd(async () => {
                const res = await getListDataApi(this.activeIndex)
                this.list.push(...res.data.data.results)
              })

            }
            .tabBar(this.TabBarBuilder(item.name, index))
          })
        }
        .scrollable(false)
        .barMode(BarMode.Scrollable)
        .divider({
          strokeWidth: 1
        })

        Row({ space: 10 }) {
          Image($r("app.media.search_icon"))
            .width(24)

            .onClick(() => {
              router.pushUrl({
                url: 'pages/SearchChannelPage'
              })
            })
          Image($r("app.media.select"))
            .width(24)
            .bindContentCover(this.isShow, this.MyChannelBuilder())
            .onClick(() => {
              this.isShow = true
            })
        }
        .justifyContent(FlexAlign.End)
        .padding({ left: 40, right: 15 })
        .height(54)
        .linearGradient({
          direction: GradientDirection.Right,
          colors: [['#00fffdfd', 0.1], ['#ffffffff', 0.3]]
        })

      }

    }
    .width('100%')
    .height('100%')
    .padding({ top: this.topheight })
  }
}